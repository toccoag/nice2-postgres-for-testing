# Postgres for use During Testing

⚠⚠⚠ Do **NOT** use when data consistency and integrity matters. ⚠⚠⚠

This image disables fsync for better performance but as a result, unclean
termination of Postgres may lead to inconsistent data and data loss.

Use [nice2-postgres] otherwise.


## TL;DR

This images is based on [nice2-postgres](https://gitlab.com/toccoag/nice2-postgres)
with additional performance enhancements and, optionally, test data.

Branches are automatically built and made available as Docker images.

**Branch naming:**

`postgres-NN`:

Docker image that contains no test data (*NN* is the postgres version)

`postgres-NN-nice-LLL-CUST`:

Docker image that contains test data from installation *CUST* version *LLL*. Postgres
version is *NN*.

**Docker image naming:**

```
registry.gitlab.com/toccoag/nice2-postgres-for-testing:${GIT_BRANCH_NAME}
```

The Docker tag name is the same as the Git branch name.


## About this Image

### User `unit_tests`

A superuser called `unit_tests` is created with password `xe94aVgIsV8W`. This credential are hardcoded in Nice.

This user was used until Nice 2.24 for running unit tests. Newer version use an embedded Postgres.


## Updating Postgres

(You must update [nice2-postgres] first.)

1. Check out the most recent version/branch:

   ```
   git checkout ${BRANCH}
   git pull
   ```

   Check out `postgres-NN` rather than a branch containing a DB.

2. Create new branch:

   ```
   git checkout -b postgres-${POSTGRES_VERSION}
   ```

3. Update version in `FROM` of `Dockerfile`.

4. Commit:

   ```
   git commit Dockerfile
   ```

5. Push branch:

   ```
   git push -u origin HEAD
   ```

6. [Schedule] weekly (re-)build

7. Go to [Protected branches] and protect newly created branch:
   - Allowed to merge: DevOps (group)
   - Allowed to push: DevOps (group)

8. Set as new [default branch]


## Adding a New Database

1. Install Git LFS:

   ```
   git lfs install
   ```

   (Install via `apt-get install git-lfs` if needed.)

2. Check out most recent branch (e.g. `postgres-22` or `postgres-23`)

   ```
   git checkout postgres-${NN}
   ```

3. Create a new branch:

   ```
   git checkout -b postgres-${PG_VERSION}-nice-${NICE_VERSION}-${SOURCE}
   ```

   Use "test" as ${SOURCE} when DB is from our internal test systems (e.g.
   from test301).

4. Create temporary DB

   ```sql
   CREATE DATABASE ${TARGET_DB};
   ```

5. Copy database:

   ```
   pg_dump -Fc -Z0 ${SOURCE_DB} | pg_restore -d ${TARGET_DB}
   ```

6. Change password of all users to `nice`:

     ```sql
     UPDATE nice_principal SET password = 'PBKDF2WithHmacSHA512:4096:512:136i33Zron48aOYDBvtbAg==:MRf3U+h9JiB5ZNEZ57VQWvCmE5yQ1IH1tTYz2ec+ZdWN/6ZtfAemXNuFd3gMLzwhy2X9e7Jt1VdmzHahInJoXA==';
     ```
7. Dump database

   ```
   pg_dump -Fc -f nice.psql ${DB_NAME}
   ```

8. Drop DB again:

   ```sql
   DROP DATABASE ${TARGET_DB}
   ```

9. Add dump to `dumps/nice.psql` to this repository.

   You can add other `dumps/${NAME}.psql` dumps in which case a db called `${NAME}` is created owned
   by user `nice`.

10. Commit change:

    ```
    git commit dumps/*.psql
    ```

11. Push changes:

    ```
    git push -u origin HEAD
    ```

12. [Schedule] weekly (re-)build


[default branch]: https://gitlab.com/toccoag/nice2-postgres-for-testing/-/settings/repository
[nice2-postgres]: https://gitlab.com/toccoag/nice2-postgres
[Protected branches]: https://gitlab.com/toccoag/nice2-postgres/-/settings/repository#js-protected-branches-settings
[Schedule]: https://gitlab.com/toccoag/nice2-postgres-for-testing/-/pipeline_schedules
