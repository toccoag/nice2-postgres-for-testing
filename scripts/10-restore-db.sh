#!/bin/bash
set -Eeu
shopt -s nullglob

cd /dumps/
for dump_file in *.psql; do
    name=${dump_file%.psql}
    echo "restoring $dump_file"
    psql -v ON_ERROR_STOP=1 -U postgres -d "$name" -c "CREATE DATABASE \"${name/\"/\"\"/}\"" || true
    time pg_restore -U nice -d "$name" --no-owner --no-acl -j 4 "$dump_file"
    psql -v ON_ERROR_STOP=1 -U postgres -d "$name" -c ANALYZE
done
