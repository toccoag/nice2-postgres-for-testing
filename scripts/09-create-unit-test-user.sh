#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<EOF
    -- Credentials are hardcoded in unit tests.
    CREATE ROLE unit_tests WITH SUPERUSER LOGIN PASSWORD 'xe94aVgIsV8W';
    CREATE DATABASE unit_tests WITH OWNER unit_tests;
EOF
